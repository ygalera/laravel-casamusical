<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="css/mistilos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 

<style>
.jumbotron {
    margin-bottom: 0px;
    background-image: url(imagenes/imagen5.jpg);
    width: auto;
    height: 200px;
    background-position: 0% 30%;
    background-size: cover;
    background-repeat: no-repeat;
    text-shadow: black 0.3em 0.3em 0.3em;
}

h1 {
    font-size: 80px;
    font-family: Jokerman;
    font-weight: bold;
    height: 80px;
   
}
</style> 
</head>
<body style="font-family: chiller; font-size: 22px;">
<div class="content"  >
<div class="jumbotron">
        
            <div class="title m-b-md" style="color:#D81B60;">
                <h1>CASA MUSICAL</h1>
            </div> 
            </div>
<nav class = "navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="/" style="font-size: 40px; color:#EC407A;">  <font style = "vertical-align: inherit;"> Casa musical  </font> </a>
  <button class = "navbar-toggler" type = "button" data-toggle = "collapse" data-target = "# navbarColor01" aria-controls = "navbarColor01" aria-expand = "false" aria-label = "Toggle navigation ">
    <span class = "navbar-toggler-icon" > </span>
  </button>

  <div class = "collapse navbar-collapse" id = "navbarColor01">
        <ul class = "navbar-nav mr-auto">
            <li>
            <a class="nav-link" href="canciones" style="font-size: 20px;">  <font style = "vertical-align: inherit;"> Canciones  </font> <span class = "sr-only">  <font style = "vertical-align: inherit;"> (actual)  </font> </ span > </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="generos" style="font-size: 20px;">  <font style = "vertical-align: inherit;"> Generos </font>  </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="album" style="font-size: 20px;"> <font style = "vertical-align: inherit;">  Album </font>  </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="autores" style="font-size: 20px;">  <font style = "vertical-align: inherit;"> Autores  </font>  </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="medios" style="font-size: 20px;"> <font style = "vertical-align: inherit;">  Medios </font>  </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="interprete" style="font-size: 20px;">  <font style = "vertical-align: inherit;"> Interpretes </font> </a>
            </li>
        </ul>
        
            
        <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}" style="font-size: 20px;">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}" style="font-size: 20px;">{{ __('Registro') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style="font-size: 20px;" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}" style="font-size: 20px;"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar sesion') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest   
            </ul>
  </div>
  </div>
 
</nav>
</div>

<div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    

    <br>
  <br>
	<footer>
		<div class="footer-container">
			<div class="footer-main">
				<div class="footer-columna">
					<h3>Suscríbete</h3>
					<input type="email" placeholder="Escriba su Correo" style="color:#D81B60;">
					<input type="submit" value=" Suscribirse">
				</div>

				<div class="footer-columna">
					<h3>Dirección</h3>
					<span class="fa fa-map-marker"><p>Cll 34a # 12b-34 Riohacha, La Guajira - Colombia</p></span>
					<span class="fa fa-phone"><p>(+57) 3008240925</p></span>
					<span class="fa fa-envelope"><p>ygalera@uniguajira.edu.co</p></span>
				</div>

				<div class="footer-columna">
					<h3>Sobre Nosotros</h3>
					<p>La Casa Musical nace en Riohacha en 2015; inspirada en una ciudad llena de música vallenata, de todas las regiones del país y fuera.</p>
				</div>
			</div>
		</div>

		<div class="footer-copy-redes">
			<div class="main-copy-redes">
				<div class="footer-copy">
					&copy; 2019, Todos los derechos reservados 
				</div>
				<div class="footer-redes">
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<a href="#" class="fa fa-youtube-play"></a>
					<a href="#" class="fa fa-github"></a>
				</div>
			</div>
		</div>
    </footer>
    
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/scripts.js"></script>

</body>
</html>
