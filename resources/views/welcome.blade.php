<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Casa Musical</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="css/mistilos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.jumbotron {
    margin-bottom: 0px;
    background-image: url(imagenes/imagen5.jpg);
    width: auto;
    height: 200px;
    background-position: 0% 30%;
    background-size: cover;
    background-repeat: no-repeat;
    text-shadow: black 0.3em 0.3em 0.3em;
}

</style>        
</head>
<body style="font-family: chiller; font-size: 25px;"> 
    

<div class="content">
        
        <div class="jumbotron">        
            <div class="title m-b-md" style="color:#D81B60;">
                CASA MUSICAL
            </div>      
        </div>
  <nav class = "navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="/" style="font-size: 40px; color:#EC407A;"> <font style = "vertical-align: inherit;"> <font style = "vertical-align: inherit;"> Casa musical </font> </font> </a>
      <button class = "navbar-toggler" type = "button" data-toggle = "collapse" data-target = "# navbarColor01" aria-controls = "navbarColor01" aria-expand = "false" aria-label = "Toggle navigation ">
        <span class = "navbar-toggler-icon"> </span>
      </button>

  <div class = "collapse navbar-collapse" id = "navbarColor01" >
        <ul class = "navbar-nav mr-auto">
            <li>
            <a class="nav-link" href="canciones" style="font-size: 20px;">  Canciones  <span class = "sr-only">  <font style = "vertical-align: inherit;"> (actual) </font> </ span > </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="generos" style="font-size: 20px;"> <font style = "vertical-align: inherit;"> Generos </font></a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="album" style="font-size: 20px;"> <font style = "vertical-align: inherit;">  Album </font></a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="autores" style="font-size: 20px;">  <font style = "vertical-align: inherit;"> Autores </font> </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="medios" style="font-size: 20px;"> <font style = "vertical-align: inherit;">  Medios</font> </a>
            </li>
            <li class = "nav-item">
            <a class="nav-link" href="interprete" style="font-size: 20px;">  <font style = "vertical-align: inherit;"> Interpretes</font> </a>
            </li>
          </ul>

      <ul class="navbar-nav ml-auto" >
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <li class = "nav-item">
                      <a class="nav-link" href="{{ url('/home') }}" style="font-size: 20px; color:#FDFEFE;">Inicio</a>
                    </li>
                    @else
                    <li class = "nav-item">
                        <a class="nav-link" href="{{ route('login') }}" style="font-size: 20px;"> <font style = "vertical-align: inherit;"> Login </font></a>

                        @if (Route::has('register'))
                        <li class = "nav-item">
                            <a class="nav-link" href="{{ route('register') }}" style="font-size: 20px;"> <font style = "vertical-align: inherit;">Registro</a>
                            </li>
                        @endif
                    @endauth
                </div>
            @endif
      </ul>

    </div>
</nav>
</div>
  <br>
  <br>

    <div class="container">
  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="imagenes/imagen9.jpg" alt="Los Angeles" style="width:100%; height:200px;">
        <div class="carousel-caption">
          <!-- <h3>Los Angeles</h3>
          <p>LA is always so much fun!</p> -->
        </div>
      </div>

      <div class="item">
        <img src="imagenes/imagen7.jpg" alt="Chicago" style="width:100%; height:200px;">
        <div class="carousel-caption">
          <!-- <h3>Chicago</h3>
          <p>Thank you, Chicago!</p> -->
        </div>
      </div>
    
      <div class="item">
        <img src="imagenes/imagen8.jpg" alt="New York" style="width:100%; height:200px;">
        <div class="carousel-caption">
          <!-- <h3>New York</h3>
          <p>We love the Big Apple!</p> -->
        </div>
      </div>

      <div class="item">
        <img src="imagenes/rio.jpg" alt="Riohacha" style="width:100%; height:200px;">
        <div class="carousel-caption">
          <h3 >Riohacha</h3>
          <p>Amamos la musica vallenata!</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>    


  <br>
  <br>
  <br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img1.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img2.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img3.jpg" style="width:200px; height:200px;"/>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img4.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img5.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img6.jpg" style="width:200px; height:200px;"/>
				</div>
			</div>
		</div>
	</div>
</div>

<br>
  <br>
  <div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img7.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img8.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img9.jpg" style="width:200px; height:200px;"/>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img10.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img11.jpg" style="width:200px; height:200px;"/>
				</div>
				<div class="col-md-4">
					<img alt="Bootstrap Image Preview" src="imagenes/img12.jpg" style="width:200px; height:200px;"/>
				</div>
			</div>
		</div>
	</div>
</div>

<br>
  <br>
	<footer>
		<div class="footer-container">
			<div class="footer-main">
				<div class="footer-columna">
					<h3>Suscríbete</h3>
					<input type="email" placeholder="Escriba su Correo" style="color:#D81B60;">
					<input type="submit" value=" Suscribirse">
				</div>

				<div class="footer-columna">
					<h3>Dirección</h3>
					<span class="fa fa-map-marker"><p>Cll 34a # 12b-34 Riohacha, La Guajira - Colombia</p></span>
					<span class="fa fa-phone"><p>(+57) 3008240925</p></span>
					<span class="fa fa-envelope"><p>ygalera@uniguajira.edu.co</p></span>
				</div>

				<div class="footer-columna">
					<h3>Sobre Nosotros</h3>
					<p>La Casa Musical nace en Riohacha en 2015; inspirada en una ciudad llena de música vallenata, de todas las regiones del país y fuera.</p>
				</div>
			</div>
		</div>

		<div class="footer-copy-redes">
			<div class="main-copy-redes">
				<div class="footer-copy">
					&copy; 2019, Todos los derechos reservados 
				</div>
				<div class="footer-redes">
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<a href="#" class="fa fa-youtube-play"></a>
					<a href="#" class="fa fa-github"></a>
				</div>
			</div>
		</div>
	</footer>

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/scripts.js"></script>

</body>
</html>
