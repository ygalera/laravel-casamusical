<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('canciones', function () {
    return view('canciones');
});

Route::get('album', function () {
    return view('album');
});

Route::get('autores', function () {
    return view('autores');
});

Route::get('interprete', function () {
    return view('interprete');
});

Route::get('medios', function () {
    return view('medios');
});

Route::get('generos', function () {
    return view('generos');
});

Route::get('login', function () {
    return view('login');
});

Route::get('register', function () {
    return view('register');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
